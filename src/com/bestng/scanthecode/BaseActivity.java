package com.bestng.scanthecode;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class BaseActivity extends Activity {
	public static Boolean isScanning = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// ??Activity???
		MyApplication.getInstance().addActivity(this);
		
		Log.e("BaseActivity","onCreate");
	}

	@Override
	protected void onDestroy() {
		Log.e("BaseActivity","onDestroy");

		super.onDestroy();
		
	}

}
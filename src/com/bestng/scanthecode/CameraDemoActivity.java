package com.bestng.scanthecode;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.TextView;

import com.bestng.scanthecode.R;
import com.google.android.glass.media.CameraManager;
import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;
import com.zxing.activity.CaptureActivity;

import java.util.ArrayList;

public class CameraDemoActivity extends BaseActivity {

	private GestureDetector mGestureDetector;
	private Context mContext = this;
	private TextView tv_screen_content;

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_camerademo);
		tv_screen_content = (TextView) findViewById(R.id.camerademo_main_content);
		// For gesture handling.
		mGestureDetector = createGestureDetector(this);

	}

	@Override
	public boolean onGenericMotionEvent(MotionEvent event) {
		if (mGestureDetector != null) {
			return mGestureDetector.onMotionEvent(event);
		}
		return false;
	}

	private GestureDetector createGestureDetector(Context context) {
		GestureDetector gestureDetector = new GestureDetector(context);
		// Create a base listener for generic gestures
		gestureDetector.setBaseListener(new GestureDetector.BaseListener() {
			@Override
			public boolean onGesture(Gesture gesture) {
				if (gesture == Gesture.TAP) {
					handleGestureTap();
					return true;
				} else if (gesture == Gesture.TWO_TAP) {
					handleGestureTwoTap();
					return true;
				} else if (gesture == Gesture.SWIPE_RIGHT) {
					handleGestureSwipeRight();
					return true;
				} else if (gesture == Gesture.SWIPE_LEFT) {
					handleGestureSwipeLeft();
					return true;
				}else if(gesture == Gesture.SWIPE_DOWN){
					//ActivityManager activityMgr= (ActivityManager) mContext.getSystemService(ACTIVITY_SERVICE);
					//activityMgr.restartPackage(());
					//activityMgr.killBackgroundProcesses(getPackageName());
					Log.e("onDestroy","ddd");

				//	onDestroy();
				}
				return false;
			}
		});
		return gestureDetector;
	}

	// Tap triggers scan ...
	private void handleGestureTap() {
		Intent openCameraIntent = new Intent(this, CaptureActivity.class);
		startActivityForResult(openCameraIntent, 0);
		BaseActivity.isScanning = true;
	}

	private void handleGestureTwoTap() {

		// Quit
		this.finish();
	}

	private void handleGestureSwipeRight() {

	}

	private void handleGestureSwipeLeft() {

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {
			BaseActivity.isScanning = false;
			Bundle bundle = data.getExtras();
			String scanResult = bundle.getString("result");
			tv_screen_content.setText(scanResult);
		}
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Log.e("onDestroy","ddd");
			MyApplication.getInstance().AppExit();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

}

package com.bestng.scanthecode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class EyeGestureReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if (BaseActivity.isScanning) {
			Log.e("", "wink");

			abortBroadcast(); // So we have a problem if we abort this
								// broadcast,
								// because we can't calibrate :(
			// The sloppy way of launching the default camera taking app
			// Intent i = new Intent("android.intent.action.CAMERA_BUTTON");
			// context.sendBroadcast(i);
		} else {
			Log.e("", "wink");

			if (getAbortBroadcast()) {

				clearAbortBroadcast();
			}

		}
	}
}